﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapComplete : MonoBehaviour {

	public GameObject LapCompleteTrig;
	public GameObject HalfLapTrig;

	public GameObject MinuteDisplay;
	public GameObject SecondDisplay;
	public GameObject MilliDisplay;

	public GameObject LapTimeBox;
int lastMin = 0;
    int lastSec = 0;
    float lastMS = 0;
    void OnTriggerEnter()
    {
        if (lastMin==0 && lastSec==0 && lastMS==0) {
            lastMin = LapTimeManager.MinuteCount;
            lastSec = LapTimeManager.SecondCount;
            lastMS = LapTimeManager.MilliCount;
            if (LapTimeManager.MinuteCount <= 9)
            {
                MinuteDisplay.GetComponent<Text>().text = "0" + LapTimeManager.MinuteCount + ".";
            }
            else
            {
                MinuteDisplay.GetComponent<Text>().text = "" + LapTimeManager.MinuteCount + ".";
            }
            if (LapTimeManager.SecondCount <= 9)
            {
                SecondDisplay.GetComponent<Text>().text = "0" + LapTimeManager.SecondCount + ".";
            }
            else
            {
                SecondDisplay.GetComponent<Text>().text = "" + LapTimeManager.SecondCount + ".";
            }
            MilliDisplay.GetComponent<Text>().text = "" + (int)LapTimeManager.MilliCount + ".";
        }
        else {
            if (LapTimeManager.MinuteCount < lastMin)
            {
                lastMin = LapTimeManager.MinuteCount;
                if (LapTimeManager.MinuteCount <= 9)
                {
                    MinuteDisplay.GetComponent<Text>().text = "0" + LapTimeManager.MinuteCount + ".";
                }
                else
                {
                    MinuteDisplay.GetComponent<Text>().text = "" + LapTimeManager.MinuteCount + ".";
                }
            }
            else if (LapTimeManager.SecondCount < lastSec)
            {
                lastSec = LapTimeManager.SecondCount;
                if (LapTimeManager.SecondCount <= 9)
                {
                    SecondDisplay.GetComponent<Text>().text = "0" + LapTimeManager.SecondCount + ".";
                }
                else
                {
                    SecondDisplay.GetComponent<Text>().text = "" + LapTimeManager.SecondCount + ".";
                }
            }
            else if (LapTimeManager.MilliCount < lastMS) {
                MilliDisplay.GetComponent<Text>().text = "" + (int)LapTimeManager.MilliCount + ".";
                lastMS = LapTimeManager.MilliCount;
            }
        }

       

     

        LapTimeManager.MinuteCount = 0;
        LapTimeManager.SecondCount = 0;
        LapTimeManager.MilliCount = 0;

        HalfLapTrig.SetActive(true);
        LapCompleteTrig.SetActive(false);
    }

}
