﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StableCam : MonoBehaviour
{
    public GameObject TheCar;
    public float CarX;
    public float CarY;
    public float CarZ;
  
   

    // Update is called once per frame
    void Update()
    {
        CarX = TheCar.transform.transform.eulerAngles.x;
        CarY = TheCar.transform.transform.eulerAngles.y;
        CarZ = TheCar.transform.transform.eulerAngles.z;

        transform.eulerAngles = new Vector3(CarX - CarX, CarY, CarZ - CarZ);

    }
}
