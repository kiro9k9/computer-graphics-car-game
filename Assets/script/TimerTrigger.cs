﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTrigger : MonoBehaviour
{
    public GameObject Timer;
    private void OnTriggerEnter()
    {
        Timer.SetActive(true);
    }
}
